Gotty
=====

*Gotty* is a getty_ framework based on letty_. The difference to classical
gettys is, that it is not a single program but a pluggable set of tools.
Whenever you need alternative functionality or additional features just implement
them and reuse the rest. Thus you get a highly configurable getty without
complex configuration files and huge binaries.

.. _getty: http://en.wikipedia.org/wiki/Getty_%28Unix%29
.. _letty: https://bitbucket.org/251/letty



Commands
--------

Gotty provides multiple basic tools:

+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| name               | usage                                                        | description                                                 |
+====================+==============================================================+=============================================================+
| g-init             | ``g-init [-c,--clear] <tty path> </path/program arguments>`` | initializes the tty given by ``<tty path>`` and optionally  |
|                    |                                                              | clears its content                                          |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-utmp             | ``g-utmp </path/program arguments>``                         | updates the utmp/wtmp file entries for the current PID and  |
|                    |                                                              | the terminal associated with ``stdin``                      |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-env              | ``g-env <TERM-value> </path/program arguments>``             | sets the environment variables ``TERM`` to ``<TERM-value>`` |
|                    |                                                              | and ``TTY`` to the terminal associated with ``stdin``       |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-out              | ``g-out <text> [/path/program arguments]``                   | writes ``<text>`` to ``stdout``                             |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-re               | ``g-re [/path/program arguments]``                           | reads up to ``GOTTY_LOGIN_NAME_MAX`` characters from        |
|                    |                                                              | ``stdin``, checks if these characters are in the portable   |
|                    |                                                              | filename character set and optionally executes              |
|                    |                                                              | ``</path/program ... input>`` with the character string as  |
|                    |                                                              | last argument                                               |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-out-v (optional) | ``g-out-v <text> [/path/program arguments]``                 | it writes ``<text>`` to ``stdout`` as well as ``g-out`` but |
|                    |                                                              | prepends:                                                   |
|                    |                                                              | ::                                                          |
|                    |                                                              |                                                             |
|                    |                                                              |     This is <nodename> (<machine>-<sysname>-<release>).     |
|                    |                                                              |                                                             |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+
| g-r (optional)     | ``g-r </path/program arguments>``                            | Both programs together reimplement ``g-re``. ``G-r`` is an  |
+--------------------+--------------------------------------------------------------+ IA32 assembler program which reads a string and executes    |
| g-ve (optional)    | ``g-ve <text> [/path/program arguments]``                    | ``g-ve`` to validate the input.                             |
+--------------------+--------------------------------------------------------------+-------------------------------------------------------------+

The last three are examples of possible customizations. Common to all commands
is the <mandatory> or [optional] execution of another command to build up the
functionality of a getty.



Configuration
-------------

Configure via macros or ``Makefile``.

+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+
| name                     | Makefile                        | default           | description                | commands                    |
+==========================+=================================+===================+============================+=============================+
| ``GOTTY_LOGIN_NAME_MAX`` | ``getconf LOGIN_NAME_MAX``      | ``256``           | maximium login name length | ``g-re``, ``g-r``, ``g-ve`` |
+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+
| ``GOTTY_UTMPX_USER``     | ``"LOGIN"``                     | ``"LOGIN"``       | ``ut_user`` in utmpx entry | ``g-utmp``                  |
+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+
| ``GOTTY_WTMP_PATH``      | ``/var/log/wtmp``               | ``/var/log/wtmp`` | path to wtmp file          | ``g-utmp``                  |
+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+
| ``GOTTY_CLEAR_SEQUENCE`` | ``"\033c"``                     | ``"\033c"``       | terminal clear sequence    | ``g-init``                  |
+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+
| ``GOTTY_USE_UPDWTMP``    | ``0``                           | ``0``             | use libc' ``updwtmp()``    | ``g-utmp``                  |
+--------------------------+---------------------------------+-------------------+----------------------------+-----------------------------+



Installation
------------

A simple ``make``, ``make install`` is enough after customizing the
``Makefile``. For the optional programs execute ``make <name>`` before the
installation step.



Usage
-----

Gettys are usually invoked by ``init(8)`` and hence the configuration depends
on the init system. A common way is to edit ``/etc/inittab``.

A basic getty:
::

    c2:2345:respawn:/sbin/g-init /dev/tty2 /sbin/g-out "login: " /sbin/g-re /bin/login


A verbose getty which clears the screen, updates utmp and sets environment variables:
::

    c2:2345:respawn:/sbin/g-init -c /dev/tty2 /sbin/g-utmp /sbin/g-env linux /sbin/g-out-v "login: " /sbin/g-re /bin/login


Maybe you prefer the 182B assembler version waiting for input:
::

    c2:2345:respawn:/sbin/g-init -c /dev/tty2 /sbin/g-out-v "login: " /sbin/g-r /sbin/g-ve /bin/login


or autologin?
::

    c2:2345:respawn:/sbin/g-init -c /dev/tty2 /sbin/g-utmp /bin/login -f <username>


Please test your configuration with ``kill -HUP 1`` before restarting.



Standards
---------

Apart from ``g-r`` and ``g-init``, all programs conform to C99 and POSIX.1-2008.
``G-r`` was written in IA32 Assembler (Intel syntax) and ``g-init`` uses
non-Posix functions like ``vhangup()``.



Compilers and Libraries
-----------------------

*Gotty* was tested with multiple C libraries (dietlibc_ 0.33, musl_ 0.9.3 (no
utmpx support), glibc_ 2.15) and compilers (clang_ 2.9, CompCert_ 1.11, gcc_
4.5.3). The binary sizes (statically linked) on the test system were:

+---------+-------------+----------+-----------+
| command | diet gcc    | musl-gcc | nasm/yasm |
+=========+=============+==========+===========+
| g-init  | 3092B       | 4740B    |           |
+---------+-------------+----------+-----------+
| g-utmp  | 5036B       | 17032B   |           |
+---------+-------------+----------+-----------+
| g-env   | 4612B       | 21124B   |           |
+---------+-------------+----------+-----------+
| g-out   | 2172B       | 4740B    |           |
+---------+-------------+----------+-----------+
| g-re    | 2364B       | 4740B    |           |
+---------+-------------+----------+-----------+
| g-out-v | 2400B       | 4740B    |           |
+---------+-------------+----------+-----------+
| g-r     |             |          | 182B      |
+---------+-------------+----------+-----------+
| g-ve    | 2348B       | 4740B    |           |
+---------+-------------+----------+-----------+

.. _dietlibc: http://www.fefe.de/dietlibc/
.. _musl: http://www.etalabs.net/musl/
.. _glibc: http://www.gnu.org/software/libc/
.. _clang: http://clang.llvm.org/
.. _CompCert: http://compcert.inria.fr/
.. _gcc: http://gcc.gnu.org/



License
-------

SimPL-2.0_

.. _SimPL-2.0: http://opensource.org/licenses/simpl-2.0



Alternatives
------------

Letty_ by the same author or agetty_, fgetty_, mingetty_, ngetty_, logind_,
among others.

::

     PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
    1900 root      20   0   156    8    4 S  0.0  0.0   0:00.00 fgetty
    1901 root      20   0   144    4    0 S  0.0  0.0   0:00.00 g-r
    1902 root      20   0  4272  188  184 S  0.0  0.0   0:00.00 agetty


.. _Letty: https://bitbucket.org/251/letty
.. _agetty: http://www.kernel.org/pub/linux/utils/util-linux/
.. _fgetty: http://www.fefe.de/fgetty/
.. _mingetty: http://sourceforge.net/projects/mingetty/
.. _ngetty: http://riemann.fmi.uni-sofia.bg/ngetty/
.. _logind: http://clientes.netvisao.pt/anbadeol/logind.html
