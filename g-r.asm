; gotty-r (g-r)
;
; author: Frank Busse
; license: SimPL-2.0
; usage: g-r </path/program arguments>
;
; Gotty-r reads a character string from stdin and executes:
; /path/program [input] arguments.
;

%ifndef GOTTY_LOGIN_NAME_MAX
        %define GOTTY_LOGIN_NAME_MAX 256
%endif
%define MAX GOTTY_LOGIN_NAME_MAX


%define origin 0x08048000



BITS 32

                org     origin

ehdr:                                            ; Elf32_Ehdr
                db      0x7F, "ELF", 1, 1, 1, 0  ;   e_ident
        times 8 db      0
                dw      2                        ;   e_type
                dw      3                        ;   e_machine
                dd      1                        ;   e_version
                dd      _start                   ;   e_entry
                dd      phdr - $$                ;   e_phoff
                dd      0                        ;   e_shoff
                dd      0                        ;   e_flags
                dw      ehdrsize                 ;   e_ehsize
                dw      phdrsize                 ;   e_phentsize
                dw      1                        ;   e_phnum
                dw      0                        ;   e_shentsize
                dw      0                        ;   e_shnum
                dw      0                        ;   e_shstrndx

ehdrsize        equ     $ - ehdr

phdr:                                            ; Elf32_Phdr
                dd      1                        ;   p_type
                dd      0                        ;   p_offset
                dd      $$                       ;   p_vaddr
                dd      $$                       ;   p_paddr
                dd      filesize                 ;   p_filesz
                dd      memsize                  ;   p_memsz
                dd      7                        ;   p_flags
                dd      0x1000                   ;   p_align

phdrsize        equ     $ - phdr

_start:
read:
                mov     eax,3                    ; read
                mov     ebx,0                    ; stdin
                mov     ecx,buf                  ; buffer
                mov     edx,MAX+1                ; size
                int     80h
                test    eax,eax
                js      err

flush:
                mov     eax,54                   ; ioctl
                mov     ebx,0                    ; stdin
                mov     ecx,21515                ; TCFLSH
                mov     edx,0                    ; TCIFLUSH
                int     80h

modstack:
                pop     ecx                      ; argc > 1?
                dec     ecx
                test    ecx, ecx
                jz      err

                mov     eax,[esp+4]              ; argv[0] = argv[1]
                mov     [esp],eax
                mov     eax,buf                  ; argv[1] = buf
                mov     [esp+4],eax

exec:
                mov     eax,11                   ; execve
                mov     ebx,[esp]                ; filename
                lea     edx,[esp+ecx*4+8]        ; env[]
                mov     ecx,esp                  ; argv[]
                int     80h

err:
                mov     eax,1                    ; exit
                mov     ebx,1                    ; 1
                int     0x80

filesize        equ     $ - $$

ABSOLUTE $

                buf     resb MAX+2               ; name + . + 0

memsize         equ     $ - origin
