/* gotty-env (g-env)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-env <TERM-value> </path/program arguments>
 *
 * Gotty-env sets the environment variables TERM to <TERM-value> and TTY to the
 * terminal associated with stdin. Afterwards it executes </path/program ...>.
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <unistd.h>


#define write_err(s) write(STDERR_FILENO, "Error: " s "\n", sizeof(s) + 7)
#define EXIT _exit(__LINE__)



int main(int argc, char * argv[]) {
	if (argc < 3) { EXIT; }

	// set TERM and TTY
	if (setenv("TERM", argv[1], 1) != 0) {
		write_err("setting TERM");
	}

	// name of tty associated with stdin
	char * tty_path;
	if (((tty_path = ttyname(STDIN_FILENO)) == NULL) || \
	    ((setenv("TTY", tty_path, 1)) != 0)) {
		write_err("setting TTY");
	}

	// execute
	execv(argv[2], &argv[2]);
	EXIT;
}
