/* gotty-out-verbose (g-out-v)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-out-v <text> [/path/program arguments]
 *
 * Gotty-out-verbose prints:
 *
 * This is <nodename> (<machine>-<sysname>-<release>.)
 *
 * followed by <text> and optionally executes [/path/program ...].
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>
#include <sys/utsname.h>
#include <unistd.h>


#define write_out(s) write(STDOUT_FILENO, s, sizeof(s) - 1) // ignore return value
#define write_out_s(s) write(STDOUT_FILENO, s, strlen(s)) // ignore return value
#define EXIT _exit(__LINE__)



int main(int argc, char * argv[]) {
	if (argc < 2) { EXIT; }

	// write info
	struct utsname uts;
	if (uname(&uts) != -1) {
		write_out("\nThis is ");
		write_out_s(uts.nodename);
		write_out(" (");
		write_out_s(uts.machine);
		write_out("-");
		write_out_s(uts.sysname);
		write_out("-");
		write_out_s(uts.release);
		write_out(").\n\n");
	}


	// write text
	write_out_s(argv[1]);

	// execute
	if (argc > 2) {
		execv(argv[2], &argv[2]);
		EXIT;
	}


	return EXIT_SUCCESS;
}
