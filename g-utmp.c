/* gotty-utmp (g-utmp)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-utmp </path/program arguments>
 *
 * Gotty-utmp updates the utmp/wtmp files for the current PID and the terminal
 * associated with stdin. Afterwards it executes </path/program ...>.
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl (no utmp support)
 */

#ifndef GOTTY_USE_UPDWTMP
	#define GOTTY_USE_UPDWTMP 0
#endif
#ifndef GOTTY_UTMPX_USER
	#define GOTTY_UTMPX_USER "LOGIN"
#endif
#ifndef GOTTY_WTMP_PATH
	#define GOTTY_WTMP_PATH "/var/log/wtmp"
#endif



// test for _Static_assert: gcc >=4.6, clang or C11
#ifndef __has_feature
	#define __has_feature(x) 0
#endif

#if !((defined __GNUC__  && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))) || \
     (defined __clang__ && __has_feature(c_static_assert)) || \
     (__STDC_VERSION__ == 201112L))
	#define _Static_assert(e,s)
#endif



#define _POSIX_C_SOURCE 200809L

#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <utmpx.h>

extern void *memccpy(void *, const void *, int, size_t);


#define write_err(s) write(STDERR_FILENO, "Error: " s "\n", sizeof(s) + 7)
#define EXIT _exit(__LINE__)



/* set_utmp_entry(tty_path, entry, pid): finds the corresponding entry in the db
 * and sets ut_user (GOTTY_UTMPX_USER), ut_line, ut_type (LOGIN_PROCESS) and tv.
 */
static inline bool set_utmp_entry(const char * const tty_path, struct utmpx * entry, const pid_t pid) {
	// select entry with our pid
	struct utmpx * e;
	while ((e = getutxent()) != NULL &&
	       (e->ut_type != INIT_PROCESS || e->ut_pid != pid)) {}

	if (e == NULL) { return false; }
	*entry = *e;


	// update values
	// - user: "implementation-defined name of the login process"
	//   [LSB10] null-terminated, truncated if need be
	_Static_assert (sizeof(entry->ut_user) > 0, "length of ut_user greater than 0");

	if (memccpy(entry->ut_user, GOTTY_UTMPX_USER, '\0', sizeof(entry->ut_user) - 1) == NULL) {
		entry->ut_user[sizeof(entry->ut_user) - 1] = '\0';
	}

	// - line: "device name"
	//   [LSB10] null-terminated, without path, truncated if need be
	_Static_assert (sizeof(entry->ut_line) > 0, "length of ut_line greater than 0");

	const char * start = (start = strrchr(tty_path, '/')) == NULL ? tty_path : start + 1;
	if (memccpy(entry->ut_line, start, '\0', sizeof(entry->ut_line) - 1) == NULL) {
		entry->ut_line[sizeof(entry->ut_line) - 1] = '\0';
	}

	// - type of entry
	//   LOGIN_PROCESS: Identifies the session leader of a logged-in user.
	entry->ut_type = LOGIN_PROCESS;

	// - time
	time_t tv;
	if ((tv = time(NULL)) == (time_t)-1) { return false; }
	entry->ut_tv.tv_sec = tv;


	return true;
}



#if !GOTTY_USE_UPDWTMP
/* write_wtmp(fd, entry): writes "entry" to the end of the file (fd), truncates
 * file to the old offset if writing failed
 */
static inline bool write_wtmp(const int fd, const struct utmpx entry) {
	// actual offset
	off_t offset = lseek(fd, 0, SEEK_END);
	if (offset == (off_t)-1) { return false; }

	// exclude huge utmpx structs ;)
	_Static_assert (sizeof(struct utmpx) <= ~(1 << (sizeof(ssize_t)*8 - 1)), "size of utmpx must fit ssize_t");

	// append entry, truncate to old offset in error case (race condition)
	ssize_t nbytes = write(fd, &entry, sizeof(struct utmpx));
	if (nbytes < (ssize_t) sizeof(struct utmpx)) {
		if ((nbytes != (ssize_t)-1) && (ftruncate(fd, offset) == -1)) {
			write_err("couldn't truncate wtmp"); }

		return false;
	}


	return true;
}
#endif



/* update_utmp_wtmp(tty_path, pid): resets utmp db, calls set_utmp_entry(),
 * updates and closes the db. If set_utmp_entry() was successful it also
 * opens the wtmp file, calls write_wtmp() and closes it. No file locking is
 * applied. Alternatively updwtmp() is called if gotty-utmp was compiled with
 * GOTTY_USE_UPDWTMP.
 */
static inline bool update_utmp_wtmp(const char * tty_path, const pid_t pid) {
	struct utmpx entry;
	bool set = false;
	bool ret = true;


	// reset, update and close utmp db
	setutxent();
	set = set_utmp_entry(tty_path, &entry, pid);
	ret = set ? pututxline(&entry) != NULL : false;
	endutxent();


	// set wtmp if set_utmp_entry() was successful
	if (set) {
#if !GOTTY_USE_UPDWTMP
		// open wtmp, append entry and close wtmp
		int fd = open(GOTTY_WTMP_PATH, O_WRONLY | O_SYNC);
		if (fd < 0) { return false; }
		ret = ret && write_wtmp(fd, entry);
		close(fd); // ignore return value
#else
		updwtmp(GOTTY_WTMP_PATH, &entry);
#endif
	}


	return ret;
}



int main(int argc, char * argv[]) {
	if (argc < 2) { EXIT; }

	// name of tty associated with stdin
	char * tty_path;
	if ((tty_path = ttyname(STDIN_FILENO)) == NULL) { EXIT; }

	// update utmp and wtmp files
	pid_t pid = getpid();
	if (!update_utmp_wtmp(tty_path, pid)) {
		write_err("updating utmp/wtmp"); }

	// execute
	execv(argv[1], &argv[1]);
	EXIT;
}
