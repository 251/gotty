GOTTY_LOGIN_NAME_MAX = $(shell getconf LOGIN_NAME_MAX)
GOTTY_UTMPX_USER = "\"LOGIN\""
GOTTY_WTMP_PATH = "\"/var/log/wtmp\""
GOTTY_CLEAR_SEQUENCE = "\"\033c"\"
GOTTY_USE_UPDWTMP = 0

CC	= diet gcc
#CC	= diet clang
#CC	= musl-gcc
#CC	= gcc
#CC	= clang
#CC	= ccomp

ASM	= nasm
#ASM	= yasm

DESTDIR =
MANDIR  = /usr/share/man/man8
CFLAGS  =


# ccomp without sse2
ifeq ($(CC),ccomp)
CFLAGS := $(CFLAGS) -fno-sse

# gcc and clang with additional flags
else
CFLAGS := $(CFLAGS) -Wall -Wextra -pedantic -Os -std=c99 -mpreferred-stack-boundary=2

# static linking for dietlibc and musl
ifeq ($(CC),musl-gcc)
CFLAGS := $(CFLAGS) -static
endif
endif

all: g-init g-utmp g-env g-out g-re

g-init: g-init.c
	@echo GOTTY_CLEAR_SEQUENCE=$(GOTTY_CLEAR_SEQUENCE)
	$(CC) $(CFLAGS) -D GOTTY_CLEAR_SEQUENCE=$(GOTTY_CLEAR_SEQUENCE) $@.c -o $@
	strip $@

g-utmp: g-utmp.c
	@echo GOTTY_UTMPX_USER=$(GOTTY_UTMPX_USER)
	@echo GOTTY_WTMP_PATH=$(GOTTY_WTMP_PATH)
	@echo GOTTY_USE_UPDWTMP=$(GOTTY_USE_UPDWTMP)
	$(CC) $(CFLAGS) -D GOTTY_UTMPX_USER=$(GOTTY_UTMPX_USER) -D GOTTY_WTMP_PATH=$(GOTTY_WTMP_PATH) -D GOTTY_USE_UPDWTMP=$(GOTTY_USE_UPDWTMP) $@.c -o $@
	strip $@

g-env: g-env.c
	$(CC) $(CFLAGS) $@.c -o $@
	strip $@

g-out: g-out.c
	$(CC) $(CFLAGS) $@.c -o $@
	strip $@

g-re: g-re.c
	@echo GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX)
	$(CC) $(CFLAGS) -D GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX) $@.c -o $@
	strip $@

g-out-v: g-out-v.c
	$(CC) $(CFLAGS) $@.c -o $@
	strip $@

g-ve: g-ve.c
	@echo GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX)
	$(CC) $(CFLAGS) -D GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX) $@.c -o $@
	strip $@

g-r: g-r.asm
	@echo GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX)
	$(ASM) -D GOTTY_LOGIN_NAME_MAX=$(GOTTY_LOGIN_NAME_MAX) -f bin $@.asm -o $@
	chmod +x $@

man: gotty.t2t
	txt2tags -o gotty.8 gotty.t2t

install:
	install -d $(DESTDIR)/sbin $(MANDIR)
	install g-init $(DESTDIR)/sbin
	install g-utmp $(DESTDIR)/sbin
	install g-env $(DESTDIR)/sbin
	install g-out $(DESTDIR)/sbin
	test -f g-out-v && install g-out-v $(DESTDIR)/sbin
	install g-re $(DESTDIR)/sbin
	test -f g-ve && install g-ve $(DESTDIR)/sbin
	test -f g-r && install g-r $(DESTDIR)/sbin
	test -f gotty.8.bz2 || bzip2 -k gotty.8
	install gotty.8.bz2 $(MANDIR)

uninstall:
	-rm -vf $(DESTDIR)/sbin/g-init
	-rm -vf $(DESTDIR)/sbin/g-utmp
	-rm -vf $(DESTDIR)/sbin/g-env
	-rm -vf $(DESTDIR)/sbin/g-out
	-rm -vf $(DESTDIR)/sbin/g-out-v
	-rm -vf $(DESTDIR)/sbin/g-re
	-rm -vf $(DESTDIR)/sbin/g-ve
	-rm -vf $(DESTDIR)/sbin/g-r
	-rm -vf $(MANDIR)/gotty.8.bz2

clean:
	@rm -vf g-init g-utmp g-env g-out g-out-v g-re g-ve g-r gotty.8.bz2 *.o
