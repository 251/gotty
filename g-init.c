/* gotty-init (g-init)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-init [-c,--clear] <tty path> </path/program arguments>
 *
 * Gotty-init initializes the tty given by <tty path>, optionally clears its
 * content and executes </path/program ...>.
 *
 * targeted OS: Linux (uses non-Posix functions like vhangup())
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#ifndef GOTTY_CLEAR_SEQUENCE
	#define GOTTY_CLEAR_SEQUENCE "\033c"
#endif


#define _POSIX_C_SOURCE 200809L

#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

extern int vhangup(void);


#define write_out(s) write(STDOUT_FILENO, s, sizeof(s) - 1)
#define EXIT _exit(__LINE__)



/* initialize_tty(tty_path, clear): opens tty_path as controlling terminal,
 * sets owner (root), group (root) and permissions (0600), redirects standard
 * streams, clears screen if "clear" is true and flushes pending input.
 */
static inline void initialize_tty(const char * const tty_path, const bool clear) {
	int fd;
	pid_t pid = getpid();

	// create a new session if we are not already session leader,
	// fails if we are process group leader but not session leader
	pid_t sid = getsid(0);
	if (sid != pid && setsid() == (pid_t) -1) { EXIT; }

	// detach controlling tty, Linux-specific
	if ((fd = open("/dev/tty", O_RDWR)) != -1) {
		if (ioctl(fd, TIOCNOTTY) == -1) { EXIT; }
		close(fd);
	}

	// open as controlling tty (first tty open, not specified by POSIX)
	if ((fd = open(tty_path, O_RDWR)) == -1) { EXIT; }
	if (isatty(fd) != 1) { EXIT; }

	// set user(root)/group(root, not tty) and permissions(0600)
	if (fchown(fd, (uid_t)0, (gid_t)0) == -1 || fchmod(fd, 0600) == -1) { EXIT; }

	// if controlling terminal is ct for other processes, steal it
	if (ioctl(fd, TIOCSCTTY, 1) == -1) { EXIT; }

	// close file descriptors, ignore return values
	close(fd); close(STDIN_FILENO); close(STDOUT_FILENO); close(STDERR_FILENO);

	// ignore SIGHUP from vhangup()
	struct sigaction sa;
	sa.sa_handler = SIG_IGN;
	sa.sa_flags   = 0;
	if (sigemptyset(&sa.sa_mask) == -1 || sigaction(SIGHUP, &sa, NULL) == -1) { EXIT; }

	// hangup
	if (vhangup() == -1) { EXIT; }

	// reset signal handling for SIGHUP
	sa.sa_handler = SIG_DFL;
	sa.sa_flags   = 0;
	if (sigemptyset(&sa.sa_mask) == -1 || sigaction(SIGHUP, &sa, NULL) == -1) { EXIT; }

	// open controlling terminal, redirect standard streams to terminal
	if ((fd = open(tty_path, O_RDWR)) == -1) { EXIT; }

	if (fd != STDIN_FILENO) {
		if (dup2(fd, STDIN_FILENO) == -1) { EXIT;
		} else { close(fd); } // ignore return value
	}

	if ((dup2(STDIN_FILENO, STDOUT_FILENO) == -1) || \
	    (dup2(STDIN_FILENO, STDERR_FILENO) == -1)) { EXIT; }

	// clear
	if (clear) { write_out(GOTTY_CLEAR_SEQUENCE); }

	// flush pending input
	tcflush(STDIN_FILENO, TCIFLUSH); // ignore return value
}



int main(int argc, char * argv[]) {
	if (argc < 3) { EXIT; }

	int arg = 1;

	// clear screen?
	bool clear = false;
	if ((strcmp(argv[arg], "-c") == 0) || (strcmp(argv[arg], "--clear") == 0)) {
		if (argc < 4) { EXIT; }
		clear = true;
		arg++;
	}

	// initialize tty
	initialize_tty(argv[arg++], clear);

	// execute
	execv(argv[arg], &argv[arg]);
	EXIT;
}
