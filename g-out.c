/* gotty-out (g-out)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-out <text> [/path/program arguments]
 *
 * Gotty-out writes <text> to stdout and optionally executes [/path/program ...].
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#define write_out_s(s) write(STDOUT_FILENO, s, strlen(s))  // ignore return value
#define EXIT _exit(__LINE__)



int main(int argc, char * argv[]) {
	if (argc < 2) { EXIT; }

	// write text
	write_out_s(argv[1]);

	// execute
	if (argc > 2) {
		execv(argv[2], &argv[2]);
		EXIT;
	}


	return EXIT_SUCCESS;
}
