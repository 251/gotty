/* gotty-ve (g-ve)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-ve <text> [/path/program arguments]
 *
 * Gotty-ve checks if the characters in <text> are in the portable filename
 * character set and if <text> is shorter than GOTTY_LOGIN_NAME_MAX.
 * Gotty-ve returns EXIT_SUCCESS or __LINE__ accordingly.
 * If additional arguments are given, it executes </path/program ... text> with
 * the valid character string as last argument, otherwise exits.
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#if !defined GOTTY_LOGIN_NAME_MAX || GOTTY_LOGIN_NAME_MAX < 1
	#define GOTTY_LOGIN_NAME_MAX 256
#endif



#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#define write_err(s) write(STDERR_FILENO, "Error: " s "\n", sizeof(s) + 7)
#define EXIT _exit(__LINE__)



/* is_in_pfcs(c): checks whether a given character c is in the portable filename
 * character set [A-Za-z0-9._-].
 */
static inline bool is_in_pfcs(char c) {
	return (((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9')) ||
	        ((c >= 'A') && (c <= 'Z')) || (c == '.') || (c == '_') ||
	        (c == '-'));
}



/* is_pfcs_s(s): checks if s is in [:pfcs:]+ and shorter than GOTTY_LOGIN_PROMPT.
 */
static inline bool is_pfcs_s(char * s) {
	size_t i = 0;
	for (; i <= GOTTY_LOGIN_NAME_MAX; i++) {
		if (!is_in_pfcs(s[i])) {
			switch (s[i]) {
				case '\n': s[i] = '\0';
				case '\0': return true;
				default:
					write_err("invalid character");
					return false;
			}
		}
	}

	write_err("illegal length");
	return false;
}



int main(int argc, char * argv[]) {
	if (argc < 2) { EXIT; }

	// validate
	if(!is_pfcs_s(argv[1])) { EXIT; }

	if (argc == 2) { return EXIT_SUCCESS; }

	// prepare argument list: /path/program args argv[1] NULL
	int i = 1;
	for (; i < argc; i++) { argv[i-1] = argv[i]; }
	argv[argc-1] = argv[0];

	// execute
	execv(argv[1], argv + 1);
}
