/* gotty-re (g-re)
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: g-re [/path/program arguments]
 *
 * Gotty-re reads up to GOTTY_LOGIN_NAME_MAX characters from stdin, checks if
 * these characters are in the portable filename character set and optionally
 * executes </path/program ... input> with the character string as last
 * argument.
 *
 * targeted OS: Posix-compliant OSs
 * tested compilers: ccomp, clang, gcc
 * tested libraries: dietlibc, glibc, musl
 */

#if !defined GOTTY_LOGIN_NAME_MAX || GOTTY_LOGIN_NAME_MAX < 1
	#define GOTTY_LOGIN_NAME_MAX 256
#endif



#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>


#define write_err(s) write(STDERR_FILENO, "Error: " s "\n", sizeof(s) + 7)
#define EXIT _exit(__LINE__)



/* is_in_pfcs(c): checks whether a given character c is in the portable filename
 * character set [A-Za-z0-9._-].
 */
static inline bool is_in_pfcs(char c) {
	return (((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9')) ||
	        ((c >= 'A') && (c <= 'Z')) || (c == '.') || (c == '_') ||
	        (c == '-'));
}



/* read_pfcs_s(): reads a string from stdin and returns it if it is in [:pfcs:]+
 * and shorter than GOTTY_LOGIN_PROMPT, otherwise exits.
 */
static inline char * read_pfcs_s(void) {
	// read
	static char pfcs_s[GOTTY_LOGIN_NAME_MAX + 1];
	if (read(STDIN_FILENO, &pfcs_s, GOTTY_LOGIN_NAME_MAX + 1) < (ssize_t)1) {
		EXIT; }

	// validate
	size_t i = 0;
	for (; i <= GOTTY_LOGIN_NAME_MAX; i++) {
		if (!is_in_pfcs(pfcs_s[i])) {
			// newline
			if (pfcs_s[i] == '\n') {
				pfcs_s[i] = '\0';
				return pfcs_s;
			// invalid character
			} else {
				write_err("invalid character");
				EXIT;
			}
		}
	}

	write_err("illegal length");
	EXIT;
}



int main(int argc, char * argv[]) {
	// read
	char * pfcs_s = read_pfcs_s();

	if (argc == 1) { return EXIT_SUCCESS; }

	// prepare argument list: /path/program args pfcs_s NULL
	int i = 1;
	for (; i < argc; i++) { argv[i-1] = argv[i]; }
	argv[argc-1] = pfcs_s;

	// execute
	execv(argv[0], argv);
	EXIT;
}
